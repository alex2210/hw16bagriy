<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 21.03.2019
	 * Time: 13:22
	 */

	/**
	 * Class Bonus
	 * @property string $title
	 * @property string $type
	 * @property string $description
	 */
	class Bonus implements Writable
	{
		protected $title;
		protected $type;
		protected $description;

		public function getTitle()
		{
			return $this->title;
		}

		public function __construct($title, $type, $description)
		{
			$this->title = $title;
			$this->type = $type;
			$this->description = $description;
		}

		public function getSummaryLine()
		{
			return  '<b>Type:</b> ' . $this->type . ',<b> Description:</b> ' . $this->description;
		}

		public function getPrice()
		{
			return '';
		}

	}