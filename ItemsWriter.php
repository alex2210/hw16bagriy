<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 21.03.2019
	 * Time: 13:23
	 */

	/**
	 * Class ItemsWriter
	 * @property Item[] $items
	 */
	class ItemsWriter

	{
		public $items = [];

		public function addItem(Writable $items)
		{
			$this->items[] = $items;
		}

		public function write()
		{
			$html = '';
			foreach ($this->items as $item){
				$item->getPrice();
				$html .= '<p class = "h4 b">'.$item->getTitle() . '</p>';
				$html .= '<ul><li>'
					. $item->getSummaryLine()
					. '</li></ul>';

			}
			return $html;
		}
	}