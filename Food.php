<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 21.03.2019
	 * Time: 12:53
	 */

	class Food extends Item
	{

		public function __construct($title, $price)
		{
			parent::__construct($title, $price);
			$this->type = self::getType('Food');
		}


		public function getPrice()
		{
			$this->price = $this->price - 10;
		}

		public function getSummaryLine()
		{
			return parent::getSummaryLine();
		}
	}