<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 21.03.2019
	 * Time: 12:54
	 */

	/**
	 * Class GoodsItem
	 * @property int $discount
	 */
	class GoodsItem extends Item
	{
		protected $discount;

		public function __construct($title, $price, $discount)
		{
			parent::__construct($title, $price);
			$this->discount = $discount;
			$this->type = self::getType('Goods');
		}

		public function getSummaryLine()
		{

			if($this->discount >= 1) {
				return parent::getSummaryLine() . ', <b>Discount:</b> ' . $this->discount . '$';
			} else {
				return parent::getSummaryLine();
			}

		}

		public function getPrice()
		{

				$this->price = $this->price - $this->discount;

		}
	}