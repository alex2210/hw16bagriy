<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 21.03.2019
	 * Time: 12:43
	 */

	/**
	 * Interface Writable
	 */
	interface Writable
	{
		public function getSummaryLine();
	}