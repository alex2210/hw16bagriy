<?php

	require_once 'Writable.php';
	require_once 'Item.php';
	require_once 'Food.php';
	require_once 'ItemsWriter.php';
	require_once 'GoodsItem.php';
	require_once 'Bonus.php';
	require_once 'menu.php';

	$item = new ItemsWriter();

	$item->addItem(
		new Food('Snickers', 20)
	);

	$item->addItem(
		new GoodsItem('Notebook', 1433, 142)
	);

	$item->addItem(
		new GoodsItem('Phone', 740, 39)
	);

	$item->addItem(
		new Food('KitKat', 32)
	);

    $item->addItem(new Bonus('Headphones ', 'Bonus','When buying a phone headphones as a gift'));


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Главная</title>
</head>
<body>
    <div class="container block">
        <p class="h2" align="center">Products</p>
        <?= $item->write() ?>
    </div>
</body>
</html>

