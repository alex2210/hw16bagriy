<?php
	/**
	 * Created by PhpStorm.
	 * User: Олексаандр
	 * Date: 21.03.2019
	 * Time: 12:44
	 */

	/**
	 * Class Item
	 * @property string $title
	 * @property string $type
	 * @property int $price
	 */
	abstract class Item implements Writable
	{
		protected $title;
		protected $type;
		protected $price;

		public function getTitle()
		{
			return $this->title;
		}

		public static function getType($type = 'base_item')
		{
			return $type;
		}

		public function __construct($title, $price)
		{
			$this->title = $title;
			$this->price = $price;
			$this->type = self::getType();
		}

		public function getSummaryLine()
		{
			return ' <b>Type:</b> ' . $this->type . ', <b>Price:</b> ' . $this->price . '$';
		}

		public abstract function getPrice();

	}